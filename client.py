import socket
import threading
import sys

class Client:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	print('Menu-Chat-Com-Python')


	def sendUsername(self):
		nick = input('Digite seu nick: ')
		self.sock.send(nick.encode('utf-8'))


	def sendMsg(self):
		while  True:
			msg = input('')
			self.sock.send(msg.encode('utf-8'))
			#self.sock.send(bytes(input(''), 'utf-8'))

	def __init__(self, address):
		self.sock.connect((address, 10000))
		self.sendUsername()
		iThread = threading.Thread(target = self.sendMsg)
		iThread.daemon = True
		iThread.start()

		while True:
			data = self.sock.recv(1024)
			if not data:
				break
			print(str(data, 'utf-8'))

client = Client(sys.argv[1])