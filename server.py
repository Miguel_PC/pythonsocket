import socket
import threading
import sys

class Server:

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	connections = []
	users = []
	def __init__(self):
		self.sock.bind(('0.0.0.0', 10000))
		self.sock.listen(1)

	def handler(self, c, a):
		while True:
			data = c.recv(1024)
			for connection in self.connections:
				connection.send(data)
			if data.decode() == 'exit':
				print(str(a[0]) + ':' + str(a[1]), "disconnected")
				self.connections.remove(c)
				c.close()
				break
			if not data:
				print(str(a[0]) + ':' + str(a[1]), "disconnected")
				self.connections.remove(c)
				c.close()
				break

	def run(self):
		while True: 
			c, a  = self.sock.accept()
			data = c.recv(1024)
			data = data.decode()
			self.users.append(data)
			#print(data.decode())
			for x in range (len(self.users)):
				print(str(self.users[x]))
			cThread = threading.Thread(target=self.handler, args=(c, a))
			cThread.daemon = True
			cThread.start()
			self.connections.append(c)
			print(str(a[0]) + ':' + str(a[1]),  "connected ")

server = Server()
server.run()